'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')
const Hash = use('Hash')

class UserSeeder {
	async run ()
	{
		await Database.table('users').insert([
			{
				username: 'roykevin',
				email: 'roy@upteamco.com',
				password: await Hash.make('thebest'),
				is_admin: 1,
				created_at: Database.fn.now(),
				updated_at: Database.fn.now()
			},
			{
				username: 'test',
				email: 'test@upteamco.com',
				password: await Hash.make('test'),
				is_admin: 0,
				created_at: Database.fn.now(),
				updated_at: Database.fn.now()
			}
		])
	}
}

module.exports = UserSeeder
