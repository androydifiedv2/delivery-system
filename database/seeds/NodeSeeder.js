'use strict'

/*
|--------------------------------------------------------------------------
| NodeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')

class NodeSeeder {
	async run ()
	{
		await Database.table('nodes').insert([
			{ node_name : 'A', created_at: Database.fn.now(), updated_at: Database.fn.now() },
			{ node_name : 'B', created_at: Database.fn.now(), updated_at: Database.fn.now() },
			{ node_name : 'C', created_at: Database.fn.now(), updated_at: Database.fn.now() },
			{ node_name : 'D', created_at: Database.fn.now(), updated_at: Database.fn.now() },
			{ node_name : 'E', created_at: Database.fn.now(), updated_at: Database.fn.now() },
			{ node_name : 'F', created_at: Database.fn.now(), updated_at: Database.fn.now() },
			{ node_name : 'G', created_at: Database.fn.now(), updated_at: Database.fn.now() },
			{ node_name : 'H', created_at: Database.fn.now(), updated_at: Database.fn.now() },
			{ node_name : 'I', created_at: Database.fn.now(), updated_at: Database.fn.now() }
		])
	}
}

module.exports = NodeSeeder
