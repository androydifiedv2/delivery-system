'use strict'

/*
|--------------------------------------------------------------------------
| NodeRouteSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')

class NodeRouteSeeder {
	async run ()
	{
		await Database.table('node_routes').insert([
			{
				"origin" : 1,
				"destination" : 3,
				"time" : 1,
				"cost" : 20,
				"created_at" : Database.fn.now(),
				"updated_at" : Database.fn.now()
			},
			{
				"origin" : 3,
				"destination" : 2,
				"time" : 1,
				"cost" : 12,
				"created_at" : Database.fn.now(),
				"updated_at" : Database.fn.now()
			},
			{
				"origin" : 1,
				"destination" : 8,
				"time" : 10,
				"cost" : 1,
				"created_at" : Database.fn.now(),
				"updated_at" : Database.fn.now()
			},
			{
				"origin" : 1,
				"destination" : 5,
				"time" : 30,
				"cost" : 5,
				"created_at" : Database.fn.now(),
				"updated_at" : Database.fn.now()
			},
			{
				"origin" : 8,
				"destination" : 5,
				"time" : 30,
				"cost" : 1,
				"created_at" : Database.fn.now(),
				"updated_at" : Database.fn.now()
			},
			{
				"origin" : 5,
				"destination" : 4,
				"time" : 3,
				"cost" : 5,
				"created_at" : Database.fn.now(),
				"updated_at" : Database.fn.now()
			},
			{
				"origin" : 4,
				"destination" : 6,
				"time" : 4,
				"cost" : 50,
				"created_at" : Database.fn.now(),
				"updated_at" : Database.fn.now()
			},
			{
				"origin" : 6,
				"destination" : 9,
				"time" : 45,
				"cost" : 50,
				"created_at" : Database.fn.now(),
				"updated_at" : Database.fn.now()
			},
			{
				"origin" : 6,
				"destination" : 7,
				"time" : 40,
				"cost" : 50,
				"created_at" : Database.fn.now(),
				"updated_at" : Database.fn.now()
			},
			{
				"origin" : 7,
				"destination" : 2,
				"time" : 64,
				"cost" : 73,
				"created_at" : Database.fn.now(),
				"updated_at" : Database.fn.now()
			},
			{
				"origin" : 9,
				"destination" : 2,
				"time" : 65,
				"cost" : 5,
				"created_at" : Database.fn.now(),
				"updated_at" : Database.fn.now()
			}
		])
	}
}

module.exports = NodeRouteSeeder
