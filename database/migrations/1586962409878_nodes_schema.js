'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NodesSchema extends Schema {
	up()
	{
		this.create('nodes', (table) => {
			table.increments()
			table.string('node_name', 10).notNullable().unique()
			table.timestamps()
		})
	}

	down()
	{
		this.drop('nodes')
	}
}

module.exports = NodesSchema
