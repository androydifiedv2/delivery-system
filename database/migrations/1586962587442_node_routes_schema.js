'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NodeRoutesSchema extends Schema {
	up ()
	{
		this.create('node_routes', (table) => {
			table.increments()
			table.integer('origin')
			table.integer('destination')
			table.float('time')
			table.float('cost')
			table.timestamps()
		})
	}

	down ()
	{
		this.drop('node_routes')
	}
}

module.exports = NodeRoutesSchema
