# Delivery Routes
This app implements a modified A* Path finding algorithm allowing not only the best path to be taken,
but shows all possible path from origin to destination

## Setup

### Tools/application needed
1. NodeJS
2. MySQL Server
3. Postman for testing

### Installation Procedure
- Install AdonisJS CLI using npm
```bash
npm i -g @adonisjs/cli
```
- Clone this repo
- Prepare your mysql database (create one!)
- Copy the provided .env.example to a new .env file
```bash
cp .env.example .env
```
- Update your mysql credentials on the .env file also the database name
- On the application root directory, run the following:
```bash
npm -i
adonis key:generate
adonis run:migration
adonis seed
```
- To Run the application:
```bash
adonis serve --dev
```

### Testing
This application uses Basic Auth, in your Postman provide the following under Authorization:
For admin:
```
username: roykevin
password: thebest
```
For non-admin
```
username: test
password: test
```
Available API endpoints

- [GET] /user
- [POST] /user
```json
{
    "username": "username",
    "password": "password",
    "email": "e@mail.com",
    "is_admin": 1
}
```
- [GET] /node
- [POST] /node
```json
{
    "node_name": "Z"
}
```
- [DELETE] /node
```json
{
    "node_name": "Z"
}
```
- [GET] /route
- [POST] /route
```json
{
    "origin" : "A",
    "destination" : "B",
    "time": 1,
    "cost" : 1
}
```
- [PUT] /route
```json
{
    "origin" : "A",
    "destination" : "B",
    "time": 2,
    "cost" : 2   
}
```
- [DELETE] /route
```json
{
    "origin" : "A",
    "destination" : "B"
}
```
- [GET] /delivery?origin=A&destination=B