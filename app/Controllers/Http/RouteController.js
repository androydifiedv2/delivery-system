'use strict'

const Route = use('App/Models/Route')
const Node = use('App/Models/Node')

class RouteController
{
    constructor()
    {
        this.arrAvailableRoutes = []
        this.arrCosts = []
        this.arrTime = []
        this.visitedNodes = []
        this.goal = null
    }

    async index({ request, response, auth })
    {
        if(auth.user.is_admin == 1)
        {
            let routes = await Route.all()
            response.json({ data: routes })
        }
        else
        {
            response.status(401).send('Access denied')
        }
    }

    async store({ request, response })
    {
        if(!auth.user.is_admin)
        {
            response.status(401).send('Access denied')
            return
        }
        let { origin, destination, cost, time } = request.all()

        let route = new Route()

        origin = (await Node.query().select('id').where('node_name', origin).first())
        destination = (await Node.query().select('id').where('node_name', destination).first())
        
        if(origin == null || destination == null)
        {
            response.json({ error: true, message: "Invalid origin or destination."})
        }
        else
        {
            //check if combination exists
            let tmpRoute = await Route.query().where('origin', origin.id).andWhere('destination', destination.id).first()

            if(tmpRoute == null)
            {
                route.origin = origin.id
                route.destination = destination.id
                route.cost = cost
                route.time = time
                
                await route.save()
    
                response.json({data: route })
            }
            else
            {
                response.json({ status: "error", message: "Route already exists."})
            }
        }
    }

    async update_route({ request, response, auth })
    {
        if(!auth.user.is_admin)
        {
            response.status(401).send('Access denied')
            return
        }

        const { origin, destination, cost, time } = request.all()

        //check if origin and destination exists
        let xOrigin = await Node.query().where('node_name', origin).first()
        let xDestination = await Node.query().where('node_name', destination).first()

        if(xOrigin == null || xDestination == null)
        {
            response.json({ status: "error", message: "Invalid origin or destination" })
            return
        }

        //get route
        let xRoute = await Route.query().where('origin', xOrigin.id).where('destination', xDestination.id).first()

        if(xRoute == null)
        {
            response.json({ status: "error", message: "This is an unknown route." })
            return
        }

        xRoute.cost = cost
        xRoute.time = time
        await xRoute.save()

        response.json({data: xRoute})
    }

    async delete_route({ request, response, auth })
    {
        if(!auth.user.is_admin)
        {
            response.status(401).send('Access denied')
            return
        }

        const { origin, destination } = request.all()

        //check if origin and destination exists
        let xOrigin = await Node.query().where('node_name', origin).first()
        let xDestination = await Node.query().where('node_name', destination).first()

        if(xOrigin == null || xDestination == null)
        {
            response.json({ status: "error", message: "Invalid origin or destination" })
            return
        }

        //get route
        let xRoute = await Route.query().where('origin', xOrigin.id).where('destination', xDestination.id).first()

        if(xRoute == null)
        {
            response.json({ status: "error", message: "This is an unknown route." })
            return
        }

        xRoute.delete()

        response.status(200).send('')
    }

    async delivery({ request , response })
    {
        let { origin, destination, sort } = request.all()

        if(origin == undefined || destination == undefined)
        {
            response.json({ status: "error", message: "Invalid origin or destination."})
            return
        }

        this.start = (await Node.query().where('node_name', origin).first())
        this.goal = (await Node.query().where('node_name', destination).first())
        
        if(this.goal == null || this.goal == null)
        {
            response.json({ status: "error", message: "Invalid origin or destination."})
        }
        else
        {
            await this.get_routes(this.start, this.goal)
            //response.json({data: this.arrAvailableRoutes })

            //format data
            let dataRoute = []
            for(let aRoutes in this.arrAvailableRoutes)
            {
                let strRoute = []
                let iTime = 0
                let iCost = 0
                for(let attr in this.arrAvailableRoutes[aRoutes])
                {
                    strRoute.push(this.arrAvailableRoutes[aRoutes][attr].node)
                    iTime += this.arrAvailableRoutes[aRoutes][attr].time ? this.arrAvailableRoutes[aRoutes][attr].time : 0
                    iCost += this.arrAvailableRoutes[aRoutes][attr].cost ? this.arrAvailableRoutes[aRoutes][attr].cost : 0
                }

                dataRoute.push({ route: strRoute.join(' -> '), totalTime: iTime, totalCost: iCost})
            }

            switch(sort)
            {
                case "cost":
                    dataRoute.sort(function(a, b) {
                        return a.totalCost - b.totalCost
                    })
                    break
                case "time":
                default:
                    dataRoute.sort(function(a, b) {
                        return a.totalTime - b.totalTime
                    })
                    break
            }
                

            response.json({ data: dataRoute })
        }
    }

    async get_routes(origin, destination)
    {
        this.visitedNodes.push(origin)
        //get allowed destinations
        let vNodes = []
        let xNodes = []
        for(let i = 0; i < this.visitedNodes.length; i++)
        {
            vNodes.push(this.visitedNodes[i].id)
            xNodes.push({ node: this.visitedNodes[i].node_name, time: this.arrTime[i], cost: this.arrCosts[i] })
        }

        if(origin.id == destination.id && xNodes.length > 2)
        {
            this.arrAvailableRoutes.push(xNodes)
        }

        let arrAllowedNodes = await Route.query().select(['id', 'origin','destination','time', 'cost']).where('origin', origin.id).whereNotIn('destination', vNodes).fetch()
        
        for(let row in arrAllowedNodes.rows)
        {
            const aNode = arrAllowedNodes.rows[row]

            this.arrCosts.push(aNode.cost)
            this.arrTime.push(aNode.time)

            let xOrigin = await Node.query().where('id', aNode.destination).first()

            await this.get_routes(xOrigin, destination);

            this.visitedNodes.pop()
            this.arrCosts.pop()
            this.arrTime.pop()
        }
    }
}

module.exports = RouteController
