'use strict'
const Node = use('App/Models/Node')
const Route = use('App/Models/Route')

class NodeController
{
    async index ( {request, response} )
    {
        if(!auth.user.is_admin)
        {
            response.status(401).send('Access denied')
            return
        }

        let nodes = await Node.all()
        response.json({data: nodes })
    }

    async store({request, response})
    {
        if(!auth.user.is_admin)
        {
            response.status(401).send('Access denied')
            return
        }

        try
        {
            let node = new Node()
            node.node_name = request.input('node_name');
            await node.save()
            response.json({ data: node })
        }
        catch(err)
        {
            if(err.code == 'ER_DUP_ENTRY')
            {
                response.json({error: true, message: 'Duplicate entry for ' + request.input('node_name')})
            }
            else
            {
                response.json({error: true, message: 'An error occured'})
            }
        }
    }

    async delete_node( { request, response, auth } )
    {
        if(!auth.user.is_admin)
        {
            response.status(401).send('Access denied')
            return
        }

        const { node_name } = request.all()

        //check if node exists
        let xNode = await Node.query().where('node_name', node_name).first()

        if(xNode == null)
        {
            response.json({ status: "error", message: "Node not found" })
            return
        }

        //delete routes first
        //node as origin
        let xRoute = await Route.query().where('origin', xNode.id).orWhere('destination', xNode.id).fetch()

        for(let route in xRoute.rows)
        {
            const xr = xRoute.rows[route]
            console.log(xr)
            xr.delete()
        }

        xNode.delete()

        response.status(200).send('')
    }
}

module.exports = NodeController
