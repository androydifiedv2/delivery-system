'use strict'
const User = use('App/Models/User')
class UserController
{
    async index({auth, response})
    {
        if(!auth.user.is_admin)
        {
            response.status(401).send('Access denied')
            return
        }

        let users = await User.all()
        response.json({ data: users })
    }

    async store( { request, response} )
    {
        if(!auth.user.is_admin)
        {
            response.status(401).send('Access denied')
            return
        }
        const { username, email, password, is_admin } = request.all();

        let user = new User()
        user.username = username
        user.email = email
        user.password = password
        user.is_admin = is_admin == '1' ? 1 : 0

        user.save()

        response.json({data: user})
    }
}

module.exports = UserController
