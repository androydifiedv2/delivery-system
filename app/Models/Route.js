'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Route extends Model
{
    static get table()
    {
        return 'node_routes'
    }
}

module.exports = Route
