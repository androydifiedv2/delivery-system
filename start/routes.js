'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('welcome')

Route.group( () => {
    Route.resource('user', 'UserController')
    
    Route.resource('node', 'NodeController')
    Route.delete('/node', 'NodeController.delete_node')

    Route.resource('route', 'RouteController')
    Route.put('/route', 'RouteController.update_route')
    Route.delete('/route', 'RouteController.delete_route')

    Route.get('/delivery', 'RouteController.delivery')
}).middleware('auth')
